import numpy as np
import matplotlib.pyplot as plt



train_rewards = np.load("lunar_lander_train.npy")
train_average_rewards = np.load("lunar_lander_train_average.npy")
test_rewards = np.load("lunar_lander_test.npy")
test_average_rewards = np.load("lunar_lander_test_average.npy")

plt.plot(train_rewards)
plt.plot(train_average_rewards, "-o")
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["Reward", "Average Reward"])
plt.savefig("train_reward.png", bbox_inches="tight")

plt.close()

plt.plot(test_rewards)
plt.plot(test_average_rewards, "-o")
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["Reward", "Average Reward"])
plt.savefig("test_reward.png", bbox_inches="tight")

plt.close()

train_rewards = np.load("lunar_lander_train_knn.npy")
train_average_rewards = np.load("lunar_lander_train_average_knn.npy")
test_rewards = np.load("lunar_lander_test_knn.npy")
test_average_rewards = np.load("lunar_lander_test_average_knn.npy")

plt.plot(train_rewards)
plt.plot(train_average_rewards)
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["Reward", "Average Reward"])
plt.savefig("train_reward_knn.png", bbox_inches="tight")

plt.close()

plt.plot(test_rewards)
plt.plot(test_average_rewards, "-o")
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["Reward", "Average Reward"])
plt.savefig("test_reward_knn.png", bbox_inches="tight")



train_rewards_70 = np.load("lunar_lander_train_70.npy")
train_average_rewards_70 = np.load("lunar_lander_train_average_70.npy")
test_rewards_70 = np.load("lunar_lander_test_70.npy")
test_average_rewards_70 = np.load("lunar_lander_test_average_70.npy")

plt.close()
plt.plot(train_rewards)
plt.plot(train_average_rewards)
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["Reward", "Average Reward"])
plt.title("Training Reward per Episode - Gamma = 0.7")
plt.savefig("train_reward_70.png", bbox_inches="tight")

plt.close()

plt.plot(test_rewards)
plt.plot(test_average_rewards, "-o")
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["Reward", "Average Reward"])
plt.title("Test Reward per Episode - Gamma = 0.7")
plt.savefig("test_reward_70.png", bbox_inches="tight")


train_rewards_80 = np.load("lunar_lander_train_80.npy")
train_average_rewards_80 = np.load("lunar_lander_train_average_80.npy")
test_rewards_80 = np.load("lunar_lander_test_80.npy")
test_average_rewards_80 = np.load("lunar_lander_test_average_80.npy")

plt.close()
plt.plot(train_rewards)
plt.plot(train_average_rewards)
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["Reward", "Average Reward"])
plt.title("Training Reward per Episode - Gamma = 0.8")
plt.savefig("train_reward_80.png", bbox_inches="tight")

plt.close()

plt.plot(test_rewards)
plt.plot(test_average_rewards, "-o")
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["Reward", "Average Reward"])
plt.title("Test Reward per Episode - Gamma = 0.8")
plt.savefig("test_reward_80.png", bbox_inches="tight")


train_rewards_09 = np.load("lunar_lander_train_90.npy")
train_average_rewards_09 = np.load("lunar_lander_train_average_90.npy")
test_rewards_09 = np.load("lunar_lander_test_90.npy")
test_average_rewards_09 = np.load("lunar_lander_test_average_90.npy")

plt.close()
plt.plot(train_rewards)
plt.plot(train_average_rewards)
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["Reward", "Average Reward"])
plt.title("Training Reward per Episode - Gamma = 0.9")
plt.savefig("train_reward_90.png", bbox_inches="tight")

plt.close()

plt.plot(test_rewards)
plt.plot(test_average_rewards, "-o")
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["Reward", "Average Reward"])
plt.title("Test Reward per Episode - Gamma = 0.9")
plt.savefig("test_reward_90.png", bbox_inches="tight")




train_average_rewards = np.load("lunar_lander_train_average.npy")
train_average_rewards_90 = np.load("lunar_lander_train_average_90.npy")
test_average_rewards = np.load("lunar_lander_test_average.npy")
test_average_rewards_90 = np.load("lunar_lander_test_average_90.npy")

plt.close()
plt.plot(train_average_rewards, "-o")
plt.plot(train_average_rewards_90, "-o")
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["0.99 Gamma", "0.9 Gamma"])
plt.title("Training Average Reward per Episode - Gamma Comparison")
plt.savefig("train_reward_comparison.png", bbox_inches="tight")


plt.close()
plt.plot(test_average_rewards, "-o")
plt.plot(test_average_rewards_90, "-o")
plt.xlabel("Episode Number")
plt.ylabel("Reward")
plt.legend(["0.99 Gamma", "0.9 Gamma"])
plt.title("Test Average Reward per Episode - Gamma Comparison")
plt.savefig("test_reward_comparison.png", bbox_inches="tight")

