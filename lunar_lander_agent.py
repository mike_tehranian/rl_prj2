import numpy as np
import gym
from sklearn.neural_network import MLPRegressor
from sklearn.neighbors import KNeighborsRegressor


NUM_ACTIONS = 4
MAX_NUM_EPISODES = 2000
MAX_TRAINING_HISTORY = 100000
NUM_STATES = 8

NUM_TRAINING_SOVLED = 9
NUM_TEST_SOVLED = 99

# One-hot encoded vector for actions:
# Nop, fire left engine, main engine, right engine
ACTION_SPACE_VECTORS = np.eye(4, dtype=np.int)

class LunarLander(object):

    def __init__(self):
        self.states = np.zeros((MAX_TRAINING_HISTORY, NUM_STATES))
        self.actions = np.zeros((MAX_TRAINING_HISTORY, NUM_ACTIONS))
        self.rewards = np.zeros((MAX_TRAINING_HISTORY, 1))
        self.next_states = np.zeros((MAX_TRAINING_HISTORY, NUM_STATES))
        self.dones = np.zeros((MAX_TRAINING_HISTORY, 1), dtype=np.int)

        self.observation_count = 0

        self.gamma = 0.99

        self.first_time_training = True
        self.training_process = True

        self.epsilon = 0.999
        self.epsilon_decay = 0.999

        self.num_training_samples = 100

        self.learner = MLPRegressor(warm_start=True,
                                hidden_layer_sizes=(100, 100),
                                learning_rate_init=2e-4,
                                max_iter=1
                                )
        self.knn_learner = KNeighborsRegressor(5, 'distance')
        # self.learner = self.knn_learner

    def get_observation_index(self):
        observation_index = self.observation_count
        if observation_index >= MAX_TRAINING_HISTORY:
            observation_index = self.observation_count % MAX_TRAINING_HISTORY
            if observation_index >= MAX_TRAINING_HISTORY or \
                    observation_index >= self.observation_count:
                print("get_observation_index- something bad happened")

        return observation_index

    def add_observation(self, state, action, reward, next_state, done):
        action_vector = ACTION_SPACE_VECTORS[action]
        observation_index = self.get_observation_index()

        self.states[observation_index, :] = state
        self.actions[observation_index, :] = action_vector
        self.rewards[observation_index, :] = reward
        self.next_states[observation_index, :] = next_state
        self.dones[observation_index, :] = done
        self.observation_count += 1

    def train_predictor(self):
        # Maybe give higher priority to more recent examples here?
        end_observation_index = self.get_observation_index()
        if end_observation_index - self.num_training_samples <= 0:
            first = list(range(MAX_TRAINING_HISTORY - self.num_training_samples - 1,
                               MAX_TRAINING_HISTORY))
            second = list(range(end_observation_index))
            indices = np.random.choice(first + second,
                                        size=self.num_training_samples)
        else:
            indices = np.random.randint(0,
                                        high=end_observation_index,
                                        size=self.num_training_samples)

        # Training data
        t_states = self.states[indices]
        t_next_states = self.next_states[indices]
        t_rewards = self.rewards[indices]
        t_actions = self.actions[indices]
        t_dones = self.dones[indices]

        if self.first_time_training:
            self.learner.partial_fit(np.hstack((t_states, t_actions)), t_rewards.squeeze())
            # self.learner.fit(np.hstack((t_states, t_actions)), t_rewards.squeeze())
            self.first_time_training = False
            return

        # Bootstrap future rewards
        predicted_next_state_rewards = np.zeros((self.num_training_samples, NUM_ACTIONS))
        for action_num in range(NUM_ACTIONS):
            action_num_set = np.repeat(ACTION_SPACE_VECTORS[[action_num]],
                                        self.num_training_samples, axis=0)
            predicted_next_state_rewards[:, action_num] = self.learner.predict(
                                            np.hstack((t_next_states, action_num_set)))

        # Add greedy policy reward with realized rewards
        predicted_next_state_rewards = t_rewards + (
                    self.gamma *
                    predicted_next_state_rewards.max(axis=1, keepdims=True) * (1.0 - t_dones)
        )

        # Update learner
        # self.learner.fit(np.hstack((t_states, t_actions)),
        self.learner.partial_fit(np.hstack((t_states, t_actions)),
                predicted_next_state_rewards.squeeze())

    def update_hyperparameters(self, episode_num):
        # Decay schedule based on number of episodes
        if episode_num < 1000:
            self.epsilon = max(0.1, self.epsilon * self.epsilon_decay)
        elif episode_num < 1500:
            self.epsilon = 0.1
        elif episode_num < 2000:
            self.epsilon = 0.05
        else:
            # At this point, try to converge
            self.epsilon = 0.01

    def predict_optimal_action(self, state, episode_num):
        if self.training_process:
            # Epsilon-greedy or not enough training data, explore and sample continuously
            if np.random.random() < self.epsilon or episode_num <= self.num_training_samples:
                return np.random.randint(NUM_ACTIONS)

            self.train_predictor()

        state_possible_actions = np.hstack((np.tile(state, (NUM_ACTIONS, 1)),
                                   np.eye(NUM_ACTIONS)))
        predicted_values = self.learner.predict(state_possible_actions)

        return np.argmax(predicted_values)

def lunar_landing_runner(lander_env, lander_agent, num_episodes, solved_limit):
    episode_rewards = []
    average_episode_rewards = []
    count_solved = 0

    for num_episode in range(num_episodes):
        state = lander_env.reset()
        episode_reward = 0
        done = False

        while not done:
            action = lander_agent.predict_optimal_action(state, num_episode)
            next_state, reward, done, _ = lander_env.step(action)
            lander_agent.add_observation(state, action, reward, next_state, done)
            episode_reward += reward
            state = next_state

        episode_rewards.append(episode_reward)
        lander_agent.update_hyperparameters(num_episode)
        reward_over_last_100 = np.mean(episode_rewards[-100:])
        average_episode_rewards.append(reward_over_last_100)

        print("Episode Number: {}, Episode Reward: {}, Average Episode Reward: {}".
                format(num_episode, episode_reward, reward_over_last_100))

        if reward_over_last_100 > 200:
            count_solved += 1
        else:
            count_solved = 0

        if count_solved > solved_limit:
            # Training/Testing complete
            return lander_agent, episode_rewards, average_episode_rewards

    return lander_agent, episode_rewards, average_episode_rewards

if __name__ == '__main__':
    np.random.seed(1729)
    lander_env = gym.make('LunarLander-v2')
    # Train
    lander_agent = LunarLander()
    lander_agent, rewards_sequence, average_episode_rewards = \
                                    lunar_landing_runner(lander_env,
                                                     lander_agent,
                                                     MAX_NUM_EPISODES,
                                                     NUM_TRAINING_SOVLED
                                                     )
    np.save("lunar_lander_train.npy", np.array(rewards_sequence))
    np.save("lunar_lander_train_average.npy", np.array(average_episode_rewards))

    # Test on 100 Episodes
    lander_agent.training_process = False
    lander_agent, rewards_sequence, average_episode_rewards = \
                                            lunar_landing_runner(lander_env,
                                               lander_agent,
                                               100,
                                               NUM_TEST_SOVLED)

    np.save("lunar_lander_test.npy", np.array(rewards_sequence))
    np.save("lunar_lander_test_average.npy", np.array(average_episode_rewards))
